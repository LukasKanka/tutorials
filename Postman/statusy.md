## STATUS CODE -- > číselná hodnota, která hovoří o tom jak dopadl můj requst 

* Web se seznamem [statusů](httpstatuses.com)  



* Statusy se dělí do pěti základních skupin 

1. 1xx Informační statusy -- > začínají 1xx 

 2. 2xx Success (Úspěch)  

 3. 3xx Redirection (přesměrování) 

 4. 4xx Client Error (chyba na straně klienta) 

 5. 5xx Server Error (chyba na straně servera) 

 