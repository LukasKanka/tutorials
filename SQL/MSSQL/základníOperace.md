## Databáze umožňuje 4 základní operace, které jsou často označovány zkratkou CRUD:
	• Create - vytvoření záznamu
	• Read - načtení (vyhledání)
	• Update - editace
	• Delete - vymazání záznamu

