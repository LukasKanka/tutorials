## Create Table - Tvorba tabulky

```
CREATE TABLE [dbo].[Uzivatele] ( 
[Id] INTNOTNULLPRIMARYKEYIDENTITY, 
[Jmeno] NVARCHAR(60) NOTNULL, 
[Prijmeni] NVARCHAR(60) NOTNULL, 
[DatumNarozeni] DATENOTNULL, 
[PocetClanku] INTNOTNULL);
```
## Drop Table - Smaže tabulku
```
DROP TABLE [Uzivatele];
```

## Delete From - Smazán
* Při mazání nezapoměň použít **WHERE** (Podmínka)
```
DELETE FROM [Uzivatele] WHERE [Id] = 2; 


DELETE FROM [Uzivatele] WHERE ([Jmeno] = 'Jan' AND [DatumNarozeni] > '1980-1-1') OR ([PocetClanku] < 3); 
```
## Update - Aktualizace dat
```
UPDATE [Uzivatele] SET [Prijmeni] = 'Dolejší', [PocetClanku] = [PocetClanku] + 1 WHERE [Id] = 1; 
```
## Insert Into - Vložit
```
INSERT INTO [Uzivatele] (
[Jmeno],
[Prijmeni],
[DatumNarozeni],
[PocetClanku])
VALUES (
'Jan', 'Novák', '1984-11-03', 17
);
```
## Where - Podmínka

* Základní operátory =, >, <, >=, <= a != určitě umíte použít. Složitější si ukážeme dále.

```
SELECT * FROM [Uzivatele] WHERE [Jmeno] = 'Jan'; 

SELECT [Prijmeni], [PocetClanku] FROM [Uzivatele] WHERE [Jmeno] = 'Jan'; 
```

* Všechny uživatele, narozené od roku 1960 a s počtem článků vyšším než 5:
```
SELECT * FROM [Uzivatele] WHERE [DatumNarozeni] >= '1960-01-01' AND [PocetClanku] > 5; 
```
## Top
Vybere prvních 10 záznamů v tabulce:
```
SELECT TOP 10 * FROM [Uzivatele];
```

## Like

* LIKE umožňuje vyhledávat textové hodnoty jen podle části textu. Funguje podobně, jako operátor = (rovná se), pouze můžeme používat 2 zástupné znaky:
	• % (procento) označuje libovolný počet libovolných znaků.
	• _ (podtržítko) označuje jeden libovolný znak.


* Pojďme si vyzkoušet několik dotazů s operátorem LIKE. Najděme příjmení lidí začínající na "S":
```
SELECT [Prijmeni] FROM [Uzivatele] WHERE  [Prijmeni] LIKE 's%';
```


* Nyní zkusme najít pětipísmenná příjmení, která mají jako 2. znak "O". Je obecně doporučováno odstraňovat v hodnotách předávaných LIKE bílé znaky. Toho docílíte funkcemi LTRIM() a RTRIM(), které odstraňují bílé znaky zleva a zprava:

```
SELECT [Prijmeni] FROM [Uzivatele] WHERE RTRIM([Prijmeni]) LIKE '_o___';

SELECT [Prijmeni] FROM [Uzivatele] WHERE [Prijmeni] LIKE '_o___';
```
## IN

* IN == IN umožňuje vyhledávat pomocí výčtu prvků. Udělejme si tedy výčet jmen a vyhledejme uživatelé s těmito jmény:
```
SELECT [Jmeno], [Prijmeni] FROM [Uzivatele] WHERE [Jmeno] IN ('Jan', 'Petr', 'Josef' );
```
## Between

* Poslední operátor, který si dnes vysvětlíme, je **BETWEEN** (tedy mezi). Není ničím jiným, než zkráceným zápisem podmínky >= AND <=. Již víme, že i datumy můžeme běžně porovnávat. Najděme si uživatele, kteří se narodili mezi lety 1980 a 1990:
```
SELECT * FROM [Uzivatele] WHERE [DatumNarozeni] BETWEEN '1980-01-01' AND '1990-01-01';
```

* Dotaz bychom mohli vylepšit porovnáváním jen roku z daného data pomocí funkce YEAR():
```
SELECT * FROM [Uzivatele] WHERE YEAR ([DatumNarozeni]) BETWEEN 1980 AND 1990;
```

## OEDER BY - Řazení

* Všechny uživatele a seřaďme je podle příjmení. Slouží k tomu klauzule ORDER BY (řadit podle), která se píše na konec dotazu (vypíše je nám podle abecedy):
```
SELECT [Jmeno], [Prijmeni] FROM [Uzivatele] ORDER BY [Prijmeni];
```
* V dotazu by samozřejmě mohlo být i WHERE (psalo by se před ORDER BY), pro jednoduchost jsme vybrali všechny uživatele.

* Řadit můžeme podle několika kritérií (sloupců). Pojďme si uživatele seřadit podle napsaných článků a ty se stejným počtem řaďme ještě podle abecedy:
```
SELECT [Jmeno], [Prijmeni], [PocetClanku] FROM [Uzivatele] ORDER BY [PocetClanku], [Prijmeni];
```
**Směr řazení:**
* Určit můžeme samozřejmě i směr řazení. Můžeme řadit vzestupně (výchozí směr) klíčovým slovem ASC a sestupně klíčovým slovem DESC. Zkusme si udělat žebříček uživatelů podle počtu článků. 
Ti první jich tedy mají nejvíce, řadit budeme sestupně. 
Ty se stejným počtem článků budeme řadit ještě podle abecedy:
```
SELECT [Jmeno], [Prijmeni], [PocetClanku] FROM [Uzivatele] ORDER BY [PocetClanku] DESC, [Prijmeni];
```
* DESC je třeba vždy uvést, vidíte, že řazení podle příjmení je normálně sestupné, protože jsme DESC napsali jen k PocetClanku.

## AS

U tabulek AS používáme ke zjednodušení operací uvnitř dotazu. U sloupců se AS používá k tomu, aby aplikace viděla data pod jiným názvem, než jsou skutečně v databázi. To může být užitečné zejména u agregačních funkcí, protože pro ně v databázi není žádný sloupec a mohlo by se nám s jejich výsledkem špatně pracovat:
```
SELECT [Jmeno], COUNT(*) AS [Pocet] FROM [Uzivatele] GROUP BY [Jmeno];
```

## Seskupování (Grouping)

Položky v databázi můžeme seskupovat podle určitých kritérii. Seskupování používáme téměř vždy spolu s agregačními funkcemi. Pojďme seskupit uživatele:
```
SELECT [Jmeno] FROM [Uzivatele] GROUP BY [Jmeno];
```
Přidejme nyní kromě jména i počet jeho zastoupení v tabulce, uděláme to pomocí agregační funkce COUNT(*):
```
SELECT [Jmeno], COUNT(*) FROM [Uzivatele] GROUP BY [Jmeno];
```

U tabulek AS používáme ke zjednodušení operací uvnitř dotazu. U sloupců se AS používá k tomu, aby aplikace viděla data pod jiným názvem, než jsou skutečně v databázi. To může být užitečné zejména u agregačních funkcí, protože pro ně v databázi není žádný sloupec a mohlo by se nám s jejich výsledkem špatně pracovat:
```
SELECT [Jmeno], COUNT(*) AS [Pocet] FROM [Uzivatele] GROUP BY [Jmeno];
```
# Agretační funkce

## Funkce COUNT - Počet 
* Příkladem takové funkce je funkce COUNT(), která vrátí počet řádků v tabulce, splňující nějaká kritéria. Spočítejme, kolik z uživatelů napsalo alespoň jeden článek:
```
SELECT COUNT(*) FROM [Uzivatele] WHERE [PocetClanku] > 0;
```
-- COUNT není příkaz, ale funkce
-- Funkce musí mít  (Závorky)
-- COUNT() přenáší jen jedno jediné číslo. Nikdy nepočítejte pomocí výběru hodnoty, pouze funkcí COUNT()!


## AVG()
**Funkce AVG()** počítá průměr z daných hodnot. Podívejme se, jaký je průměrný počet článků na uživatele:
```
SELECT AVG([PocetClanku]) FROM [Uzivatele];
```
-- Vypocital pocět článku na uživatele


## SUM() 
**Funkce SUM()** vrací součet hodnot. Podívejme se, kolik článků napsali dohromady lidé narození po roce 1980:
```
SELECT SUM([PocetClanku]) FROM [Uzivatele] WHERE [DatumNarozeni] > '1980-12-31';
```
-- počet článků napsali dohromady lidé narození po roce 1980.

## MIN() 
**Funkce MIN()** vrátí minimum (nejmenší hodnotu). Najděme nejnižší datum narození:
```
SELECT MIN([DatumNarozeni]) FROM [Uzivatele];
```
-- Najde nejnižší datum narozeni.
--Pozor, pokud bychom chtěli vybrat i jméno a příjmení, tento kód nebude fungovat:
```
-- SELECT [jmeno], [prijmeni], MIN([DatumNarozeni]) FROM [Uzivatele];
```

* Agregační funkce pracuje s hodnotami více sloupců a vybrané sloupce (Jmeno a Prijmeni) nebudou nijak souviset s hodnotou MIN(). Problém bychom mohli vyřešit pod dotazem nebo ještě jednodušeji se funkcím MIN() a MAX() úplně vyhnout a použít místo nich řazení a TOP:
```
SELECT TOP 1 [Jmeno], [DatumNarozeni] FROM [Uzivatele] ORDER BY [DatumNarozeni];
```
## MAX() 
* Obdobně jako MIN() existuje i funkce MAX(). Najděme maximální počet článků od 1 uživatele:
```
SELECT MAX([PocetClanku]) FROM [Uzivatele];
```
-- Najde maximální počet článku od jednoho uživatele.

# Datové typy
Hned na začátku našeho on-line kurzu MS-SQL jsme si uvedli několik základních datových typů. Tehdy jsem vám s nimi nechtěl motat hlavu. Řeč byla o typech INT, VARCHAR a DATE. Databáze (konkrétně zde MS-SQL) jich má samozřejmě mnohem více. Uveďme si několik tabulek a představme si ty nejdůležitější.

![](obr1.png)

![](obr2.png)

* Nová slova upřesňující datový typ se vkládají za něj, stejně jako tomu bylo u IDENTITY. Uveďme si nějaký příklad:
```
CREATE TABLE [Osoby] (
    [Id] INT NOT NULL IDENTITY(1, 1),
    [Jmeno] CHAR(35) NOT NULL,
    PRIMARY KEY ([Id])
);
```
Tip na závěr:
```
username VARCHAR (128) UNIQUE NOT NULL, UNIQUE = jedinečný záznam nemůže být nikde stejný
```