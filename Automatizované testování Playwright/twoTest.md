## Test s popisem všech funkcí

```
// Do soboru na importujeme browser 

import { chromium } from "@playwright/test"; 

// Zapíšeme Konstruktor který se vždy spustí na začátku testu 

(async () =>{ 

   // v něm zpustíme browser a přiřadíme ho k příslušné proměnné pomocí launch 

   // před každe volání proměnné použijeme await 

   // pomocí headless: false - zobrazíme v testu prohlížeč 

 const browser = await chromium.launch({ headless: false }); 

 //  Contextu můžeme spustit libovolné množství v rámci jednoho browseru a jsou od sebe izolované 

 // zde jsme upravili velikost okna 

 const context = await browser.newContext({  

    viewport: {width: 1920, height: 1080 } , 

  }); 

  // zde pomocí contextu jsme otevřeli stránku 

 const page = await context.newPage() 

  // co nastavíme všechny contexty, mužeme psát samotný test 

 // pomocí metody goto poskytneme URL jako argument 

 await page.goto("https://the-internet.herokuapp.com", { 

   // volitelné argumenty 

    waitUntil: "domcontentloaded", // udává na jaký ze tří událost má čekat než bude stránku považovat za načtenou 

    timeout: 10000, // argument timeout určuje čas za jakou dobu se stránka načte a pokud to bude trvat déle vyhodí chybu 

 }); 

 // Touto metodou zavřeme browser 

 await browser.close() 

})(); 

// sript spustíme node a cesta/nazev souboru 

// při chybě musíme v package.json přidat řádek "type". module 

```