## Otázky a odpovědí k Playwright
**Téma otázek**

1-8 -- > Úvod 

9-13 -- > První skript 

14-19- Elementy interakce 

20-22--> Locatory 

23-27 -- > Eventy 

28-31 --> Práce s více stránkami browseru 

32 -- > iframes 

33-37 -- > Debugging 

38-44 -- > Runner konfigurace 

45-53 -- > Runner - jak psát základní testy 

54-55 -- > Jak psát testy v Playwright s využitím fixtures 

56-58 -- > Testovací artefakty a kde je najdeme 

59-62 -- > Runner visuální porovnání 

**Otázky odpovědi**

1. Co je Playwrigt? -- > Knihovna pro automatizaci testů 

1. Jaký je originální jazyk Playerightu? -- > JavaScript 

1. Které jazyky playwright podporuje? -- > JavaScript, Python, TypeScript, .Net 

1. Co umí Playwright? -- > E2E testování, Cross-browser testování, Cross- doménové testování 

1. Co je hlavní výhodou Playwright? -- >  Jednotné API pro všechny podporované Browsery 

1. Playwright je : ? -- > Asynchronní 

1. Která klíčová slova je nutné používat při psaní kódu? -- > Await, Async 

1. Které klíčové slovo pozastaví provádění kódu do té doby, než se asynchroní funkce provede? -- > Await 

1. Jaká je zkratka JavaScriptové funkce, která se spustí hned, jakmile je definována? -- > IIFE 

1. Vyber správné (nejlepší) volání pro otevření nové stránky -- > const page = await context.newPage() 

1. Který argument definuje událost, na kterou bude metoda čekat než bude považovat stránku za načtenou? -- >  waitUntil 

1. Jaké zásadní proměnné definujeme hned v úvodu scriptu? -- > browser, context, page  

1. Kterým příkazem dojde k zavření stránky? -- >  await browser.close() 

1. Co vykoná metoda page.waitForTimeout(300) -- > pozastaví exekuci skriptu na dobo 3000ms 

1. Veškeré interakce lze provádět těmito způsoby: (více správných odpovědí) -- > použitím metody objektu reprezentující stránku,    použitím metody objektu reprezentující element 

1. Jaké jsou metody pro práci s checkboxem? (více správných odpovědí) -- > check, uncheck 

1. Který selector vybere první element z pole elementů? -- > nth=0 

1. Vlastní selector Playwrightu nth=-1 (v případě, že se jedná o pole s více elementy), mi to vrátí: -- > Poslední hodnotu 

1. Metoda fill má tyto povinné argumenty: (více správných odpovědí) -- > selektor, hodnota pro vyplnění pole 

1. Co je to lokátor? -- > Představuje logiku, jak získat aktuální reprezentaci elementu webové stránky 

1. Vyberte metodu pomocí které se lokátor vytváří: -- > page.locator() 

1. Pro metodu page.locator() platí: (více správných odpovědí) -- > je tzv. striktní: selektoru musí odpovídat pouze konkrétní unikátní element,   přijme pouze jeden argument 

1. Umožňuje Playwright „naslouchat a reagovat“ na událostí odehrávajících se v rámci stránky nebo browseru, jako např. síťové requesty nebo otevření nové stránky? -- > Ano, a je důležité, v jakém pořadí jsou kódy ve snippetu napsané. 

1. Co je race condition? -- > chyba v systému nebo procesu, ve kterém jsou výsledky nepředvídatelné při nesprávném pořadí nebo načasování jeho jednotlivích operací. 

1. Vyber správné pořadí příkladů: -- > "page.waitForReqeuest() page.goto(""""")" 

1. K čemu slouží metoda page.on()? -- > k naslouchání specifického eventu, který se zadá jako argument metody 

1. K čemu slouží metoda JSON.stringify()? -- > převede objekt nebo hodnotu JavaScriptu na řetězec JSON 

1. Při vytvoření nové stránky se jako metoda pro čekání použije: -- > context.waitForEvent("page") 

1. Jaká metoda slouží k přepínání mezi dvěma stránkami? -- > žádná se nepoužívá, v playwright můžeme pracovat s oběma najednou 

1. K čemu slouží metoda console.log()? -- >  k vypsání výsledků do konsole 

1. Která metoda se použije pro zpomalení exekuce? -- >  waitForTimeout 

1. Která metoda slouží k získání iframe? -- > page.frame() 

1. K čemu se používá nástroj Playwright Inspector? --> Napomáhá při identifikaci a debuggingu chyb 

1. Jaké možnosti nabízí Playwright pro debugging? (více správných odpovědí) -- >  Playwright Inspector, Prostřednictvím developer console, Vnitřním logováním Playwrightu do console 

1. Jak nastavíme enviromentální proměnnou pro aktivaci Playwright inspector? -- > PWDEBUG=1 

1. Kterou hodnotu musíme přiřadit enviromentální proměnné pro zapnutí debuggingu v automatizovaném browseru? -- > PWDEBUG="console" 

1. Jakým způsobem zobrazíme vnitřní logování Playwrightu do konzole? -- > PWDEBUG="pw:api" 

1. Jak se jmenuje nativní test runner Playwrightu? --> Playwright Test 

1. Kdy použijeme test runner Playwright test? -- > Testování webových stránek a aplikací 

1. Co je hlavní výhodou Playwright testu? -- > Nativní test runner, který je max kompaktibilní s playwright automatizační knihovnou a jeho požitím se např. o startování browseru, kontextů a stránek apod. nemusíte starat. 

1. Jak musí být pojmenován konfigurační soubor, aby ho po umístění do kořene projektu testrunner automaticky identifikoval? -- > playwright.config.js 

1. Jakou hodnotu musí mít property headless, pokud chceme sledovat browser při exekuci? -- > true 

1. Jak se nazývají testovací konfigurace, které vytváříme v konfiguračním souboru? -- > projects 

1. Co určují workers? -- > Maximální počet paralelně exekuovaných souborů s testy 

1. Kterým příkazem v CMD se spouští testy? -- > npx playwright test 

1. Soubory, které obsahují testy, či scénáře s testy, které má Playwright Test runner spustit, se pojmenovávají: -- > [jmeno souboru].spec.js 

1. Callback funkce je: -- > 1. poskytnuta jiná funkce jako argument a je spuštěna až po splnění určité podmínky v rámci funkce, která ji používá. 2. často se píše jako anonymní funkce jako argument funkce, která ji používá. 

1. K čemu slouží metoda test.describe? -- > K rozčlenění testů do testovacích suit 

1. Co je test objekt? -- > Slouží k psaní testů a volání metod souvisejících s exekucí testů. 

1. Co je expect objekt? -- > Knihovna k verifikaci asertů. 

1. Jaké 2 argumenty přijímá testovací funkce? -- > Název testu a call back function 

1. Jakou metodu musím v rámci metody describe doplnit, aby nebyly testy spouštěny sekvenčně: -- > parallel 

1. Jakou metodou lze exekuci testů provádět souběžně? -- > parallel 

1. Co jsou fixtures? (více správných odpovědí) -- > Kousky kódu, funkce, stringy či jiné datové typy, které umožní velmi flexibilně konfigurovat testy 

1. Který příkaz v CMD slouží ke spuštění testů s fixtures, pokud jsou označeny tagem "@withfixture"? -- > npx playwright test --grep=@withfixture 

1. Co je Playwright Test Viewer? -- > Nástroj umožnující prohlédnout si zaznamenané traces 

1. Jaké testovací artefakty můžeme definovat? (více správných odpovědí) -- > Video, Screenshots, Trace 

1. Co vše je možné v Playwright Test Viewer zobrazit? (více správných odpovědí) --  > Screenshoty, seznam všech akcí které proběhly, časová historie událostí při exekuci testu 

1. Která metoda slouží pro vizuální testování, kdy dochází k porovnání snapshotů vygenerovaných při první exekuci se snapshoty z následných exekucí -- > toMatchSnapshot() 

1. Jakou metodu získáme screenshot? -- > screenshot() 

1. Použitím jaké metody získáme název stránky? -- > title() 

1. Jakým způsobem se snapshoty aktualizují? -- > Příkazem npx playwright test --update-snapshots 

