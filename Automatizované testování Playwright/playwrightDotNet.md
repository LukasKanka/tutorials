## Playwright MSTest + .Net 


**Instalace** 

Playwright byl vytvořen speciálně pro potřeby end-to-end testování. Playwright podporuje všechny moderní renderovací enginy včetně Chromium, WebKit a Firefox. Testujte v systémech Windows, Linux a macOS, lokálně nebo na CI, bezobslužně nebo v čele s nativní mobilní emulací. 

Můžete se rozhodnout použít [základní třídy NUnit](https://playwright.dev/dotnet/docs/test-runners#nunit) nebo [základní třídy MSTest](https://playwright.dev/dotnet/docs/test-runners#nunit), které Playwright poskytuje pro psaní komplexních testů. Tyto třídy podporují spouštění testů na více modulech prohlížeče, paralelní testy, úpravu možností spuštění/kontextu a získání instance [Page/BrowserContext](https://playwright.dev/dotnet/docs/api/class-browsercontext) na test z krabice. Alternativně můžete [knihovnu](https://playwright.dev/dotnet/docs/library) použít k ručnímu zápisu testovací infrastruktury. 

* Začněte vytvořením nového projektu pomocí rozhraní . Tím se vytvoří adresář, který obsahuje soubor: `dotnet newPlaywrightTestsUnitTest1.cs `



```dotnet new mstest -n PlaywrightTests 
cd PlaywrightTests 
``` 

Nainstalujte potřebné závislosti Playwright: 


```
dotnet add package Microsoft.Playwright.MSTest 
``` 

Sestavte projekt tak, aby byl k dispozici uvnitř adresáře: `playwright.ps1bin `
```
dotnet build 
``` 

Nainstalujte požadované prohlížeče nahrazením skutečným názvem výstupní složky, např.: `netXnet6.0 `
```
pwsh bin/Debug/net7.0/playwright.ps1 install 
``` 

Pokud není k dispozici, musíte nainstalovat **PowerShell.pwsh** 

Přidání příkladů testů 

Upravte soubor pomocí níže uvedeného kódu a vytvořte příklad kompletního testu: `UnitTest1.cs` 

```
using System.Text.RegularExpressions; 
using System.Threading.Tasks; 
using Microsoft.Playwright; 
using Microsoft.Playwright.MSTest; 
using Microsoft.VisualStudio.TestTools.UnitTesting; 
 
namespace PlaywrightTests; 
 
[TestClass] 
public class UnitTest1 : PageTest 
{ 
    [TestMethod] 
    public async Task HomepageHasPlaywrightInTitleAndGetStartedLinkLinkingtoTheIntroPage() 
    { 
        await Page.GotoAsync("https://playwright.dev"); 
 
        // Expect a title "to contain" a substring. 
        await Expect(Page).ToHaveTitleAsync(new Regex("Playwright")); 
 
        // create a locator 
        var getStarted = Page.GetByRole(AriaRole.Link, new() { Name = "Get started" }); 
 
        // Expect an attribute "to be strictly equal" to the value. 
        await Expect(getStarted).ToHaveAttributeAsync("href", "/docs/intro"); 
 
        // Click the get started link. 
        await getStarted.ClickAsync(); 
 
        // Expects the URL to contain intro. 
        await Expect(Page).ToHaveURLAsync(new Regex(".*intro")); 
    } 
} 
``` 

Spuštění ukázkových testů 

Ve výchozím nastavení budou testy spuštěny na chromu. To lze konfigurovat pomocí proměnné prostředí nebo úpravou [možností konfigurace spuštění](https://playwright.dev/dotnet/docs/test-runners). Testy jsou spouštěny v bezobslužném režimu, což znamená, že se při spuštění testů neotevře žádný prohlížeč. Výsledky testů a protokoly testů se zobrazí v terminálu.BROWSER 



dotnet test -- MSTest.Parallelize.Workers=5 
 

Podívejte se na náš dokument o [Test Runners](https://playwright.dev/dotnet/docs/test-runners), kde se dozvíte více o spouštění testů v režimu hlavy, spouštění více testů, spouštění konkrétních konfigurací atd. 