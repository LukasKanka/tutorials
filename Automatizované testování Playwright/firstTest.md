## První test

* základní a jednoduchý test na webu [Google](https:/google.com)

```
import { test, expect } from '@playwright/test'; 

import { chromium } from '@playwright/test'; 

 
 

( async () => { 

 
 

const browser = await chromium.launch(); 

const context = await browser.newContext(); 

const page = await context.newPage(); 

 
 

await page.goto('https://google.com'); 

 
 

// Odsouhlasí cookies 

await page.click('#L2AGLb'); 

 
 

// Klikněte na pole pro vyhledávání. 

const searchInput = await page.$('[name="q"]'); 

await searchInput?.type('test'); 

 
 

// Potvrdíme vyhledávání stisknutím klávesy Enter 

await searchInput?.press('Enter'); 

 
 

await page.waitForNavigation(); 

console.log('Search results page title:', await page.title()); 

 
 

 
 

// Vypíše název stránky do konzole 

const title = await page.title(); 

console.log(title); 

 
 
// Zavře prohlížeč kde probíhal test
await browser.close(); 

 
 
 

}); 

```