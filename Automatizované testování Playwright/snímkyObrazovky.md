## Snímky obrozavky během testů
* je několik způsobů jak dělat snímky obrazovky během testů

Zde je rychlý způsob, jak zachytit snímek obrazovky a uložit jej do souboru: 
```
await page.screenshot({ path: 'screenshot.png' }); 
```


* Celostránkové snímky obrazovky 

Snímek obrazovky celé stránky je snímek obrazovky celé rolovací stránky, jako byste měli velmi vysokou obrazovku a stránka by se do ní mohla zcela vejít. 
```
await page.screenshot({ path: 'screenshot.png', fullPage: true }); 
```
 

* Zachycení do vyrovnávací paměti 

Spíše než zapisovat do souboru, můžete získat vyrovnávací paměť s obrazem a následně jej zpracovat nebo jej předat zařízení pro rozdíl pixelů třetí strany. 
```
const buffer = await page.screenshot(); 

console.log(buffer.toString('base64')); 
```
 

* Snímek obrazovky prvku 

Někdy je užitečné pořídit snímek obrazovky jednoho prvku. 
```
await page.locator('.header').screenshot({ path: 'screenshot.png' }); 
```