**Nejrychlejší způsob instalace Playwright v VSCOde**
* Jdi do složky projektu pomocí CMD / terminálu a tam zadej: 

```
npm init playwright@latest
```

* Projdi instalací **Playwright**

```
Run the install command and select the following to get started:

Choose between TypeScript or JavaScript (default is TypeScript)
Name of your Tests folder (default is tests or e2e if you already have a tests folder in your project)
Add a GitHub Actions workflow to easily run tests on CI
Install Playwright browsers (default is true)
```

* Test tímto způsobem instalace spustíme pomocí:

```
npx playwright test
```