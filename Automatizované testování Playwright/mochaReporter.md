## Mocha-html-reporter 

* Mocha-html-reporter je knihovna, která umožňuje generovat HTML reporty pro testy napsané pomocí Mocha frameworku (který lze použít i s Playwrightem). 
* Pro instalaci stačí spustit příkaz: 

 
```
npm install mocha mocha-html-reporter --save-dev 
```
 

 * Poté je třeba upravit konfigurační soubor pro Mocha a přidat plugin pro generování HTML reportů: 

 
```
const Mocha = require('mocha'); 
 

const mocha = new Mocha 

({ 
 

  reporter: 'mocha-html-reporter', 

 
  reporterOptions: { 
     

reportDir: './test-reports', 
 

    reportName: 'test-report', 

 
    reportTitle: 'Test Report' 
  

 } 
}); 
```
 

* Poté je možné spustit testy pomocí Mocha a HTML, report se vygeneruje automaticky. 
 vložíme:

 
```
// Vytvořte HTML soubor pro report 

const fs = require('fs'); 

const date = new Date().toISOString().substring(0, 19).replace('T', ' '); 

const reportFileName = `report-${date}.html`; 

const reportFile = fs.createWriteStream(reportFileName); 

reportFile.write('<html>https://zive.cz<body>'); 

reportFile.write('<h1>Example Report</h1>'); 

reportFile.write(`<p>Tested at ${date}</p>`); 

reportFile.write('</body></html>'); 

reportFile.end(); 
```
* Výsledné reporty můžeme nastavit v mnoha parametrech než v této ukázce kódu. Toto je úplný základ. Více najdete v dokumentaci [Mocha](https://mochajs.org/).