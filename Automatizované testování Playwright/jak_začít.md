**Založení nového projektu**

* Zde je návod jak nainstalovat **Playwright** jako v freamork k JavaScriptu či TypeScriptu. **Playwright** navíc podporuje Python, Javu a .Net.

* Jako první doporučuji naštívit Officiální web [Playwright enables reliable end-to-end testing for modern web apps.](https://playwright.dev/) Kde najdete skvělou dokumentaci k **Playwright**.

* Je nutné stáhnout a nainstalovat [Node.js](https://nodejs.org/en)

* Do VSCode stáhnout Extensions [Playwright Test for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright)

* mkdir vytvoří v cmd / terminálu složku pro nový projekt, v místě kde se nacházíme

* cd název nového projektu nás přesune do složky nového projektu

* První dva body je možné udělat v GUI pomocí průzkumníka a pak otevřít terminál v tomto místě a do terminálu už jen vložit `npm init -y ` , tím si připravíme projekt.



```
mkdir novy_projekt 

cd novy_projekt 

npm init -y 
```


.................................................................... 
* Tímto nainstalujeme samotný **Playwright**.

```
npm i playwright
```

* Instalace nás provede, nastavením jako jestli chcete psát kód v JavaScriptu nebo TypeScriptu.
* Po instalaci můžeme začít psát samotný test.

................................................................... 

* Test spustíme v terminálu v místě testu pomocí:

``` 
node test.js 
```