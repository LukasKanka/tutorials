## Spouštěč testů do VS Code 


**Pod Windows:** 

 

Vytvoříme soubor `bash.bat` v kořenovám adresáři projektu

 

**Do něj vložíme:** 

 
```
@echo off 

dotnet test --logger:"html;LogFilePath=testresults.html;" 
```
 

**Pod Linux:**

 

Vytvořte nový textový soubor s příponou .sh. 

Do souboru vložte následující příkaz: 
```
bash 

Copy code 

#!/bin/bash 

dotnet test --logger:"html;LogFilePath=testresults.html" 
```

Uložte soubor a nastavte oprávnění pro spuštění (např. chmod +x název_souboru.sh). 

 

Tento soubor script bude spouštět testy a uloží výsledek do složky TestResults v kořenovém adresáři v html. 

 

Na spoštění testů si můžeme stáhnout plugin Batch Runner. Pak stačí kliknout na bash pravím tlačítkem a kliknout na Run Batch File. Tím spustíme test i s logováním do html. 

Toto funguje pod MSTest, Nunit a xUnit. 

Pokud má být výsledek testu bez reportu stačí odmazat: --logger:"html;LogFilePath=testresults.html"

 

Dlaší ze způsobů logování je možné použít knihovnu Nlog. 

