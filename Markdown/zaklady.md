Ve **VS Code** používej náhled, tlačítko vpravo nahoře. 

**Odstavec**  

Stačí udělat prázdný řádek.

**Zvýraznění textu:**  tučný text -- >  **dvě hvězdičky na začátku a konci** nebo __dvě podtržítka na začátku a konci__ 

*Zvýraznění textu:* kurzíva -- >  *jedna hvězdička na začátku i konci* nebo _jedno podtržítko na začátku a konci_ 

**Seznamy** -- > na začátku řádku napsat hvězdičku nebo mínus  * / - 

* AAA
* BBB
* CCC

Vnořené seznamy stačí odsadit tabulatorem.

Číslované seznamy: na začátek napsat číslo a tečku, začneme jedničkou. Je jedno jaké dáváme další čísla, Markdown je vkládá od 1 postupně dál.

1. Jedna
1. Dva
3. Tři

Seznam vnořený stačí odsadit.  

Oba seznamy se dají kombinovat 

**Nadpisy** -- >  Na začátku řádku: stačí jeden nebo více hastagu. 

# Nadpis první úrovně 

## Nadpis druhé úrovně 

### Nadpis třetí úrovně  

#### A tak dále 

**Citace**

> Citace -- > pro citaci na začátek zapíšeme (větší než ) ">"  . 
>
>  Má-li více odstavců , musí být  označený  vynechaný řádek. 



**Odkazy** -- > Automaticky rozezná odkaz https://www.lukan.cz/. 

Pokud chcete mít text jako odkaz -- > vložte text do [hranatých závorek a odkaz za něj do kulatých závorek](https://www.lukan.cz/.) 


**Obrázky**  -- > ![váš obrázek](obrázek.cz) v hranaté závorce je text pokud nelze zobrazit obrázek, hranatá závorka může zůstat prázdná. V kulaté je cesta k obrázku. Pokud obrázek bude ve složce kde je Markdown tak stačí pouze název obrázku a koncovka např. obrazek.png

**Zdrojový kód** -- > uzavírá se do zpětných apostrofů `(zpětný apostrof uděláme levý alt + 96).` 

**Zdrojový kód na více řádku** -- > Použijeme tři zpětné apostrofy na samotném řádku,  text není třeba odsazovat. 

```
// Ukázka Zdrojového kódu na více řádků
import { chromium } from "@playwright/test";

(async () => {
    const browser = await chromium.launch({ headless: false });
    const context = await browser.newContext({ 
        viewport: {width: 1920, height: 1080 } ,
    });
    const page = await context.newPage();

```

**HTML**
* Do Markdown můžeme přímo zapisovat HTML a ten se vám zobrazí jako na webové stránce.

<div style="color: red; background-color: blue; font-size: 20px; text-align: center">
<h2>Čus Sporťák</h2>
</div>

* HTML v Markdown vždy nemusí fungovat, ale mám odzkoušené že funguje v VSCode tak i v editoru Joplin.

**Tabulky** 
 *  Svislým znakem oddělíme sloupec: | levý alt + W
 *  Mínusem uděláme tučnější čáru: ---

Výkon | Pondělí | Úterý | Středa
--- | --- | --- | ---
Franta | 20% | 80% | 0%
Kolohlava | 100% | 100% | 99,7%

* Navíc Buňky v tabulce může zarovnávat do stran, ale o tom si více s dalšími triky povíme v dalším díle.




