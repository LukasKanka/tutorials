## Odkaz na ikony pro váš web 

[ikony](http://www.iconfinder.com )

## Ikona stránky (v liště prohlížeče)

Do hlavičky vložit:
```
<link rel="shortcut icon" href="obrazky/logo.png" /> 
```
Ikony doporučuji vložit do vlastní složky, kde pak na ně odkazovat ve funkci link.