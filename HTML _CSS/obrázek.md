## JAk vložit do stránky obrázek

```
<img src="obrazky/cesta_k_obrazku.png" width="350" height="200" alt="Název_obrázku"> 
```
U vloženého obrázku můžeme měnit více parametrů.

## Obrázek přes celou stránku css 

```
body { 

    background-image: url(obrazky/dviakq-f06cc2b3-8ca6-41f9-b6c3-6fb9a58e1bf7.jpg); 

    background-size: 100%; 

    /* obrázek na pozadí */ 

    /*background-attachment: fixed;*/ 

    /* pozadí neroluje */ 

    /* background-color: black;   případná barva pozadí černá */ 

    /*color: white; } /* bílý text */ 

} 
```

## Jak vložit do HTML obrázek s odkazem příklad v praxi


```
</center> 

    <center> 

        <table border="2"> 

            <tr> 

                <td>><a href="clanky/pixelfed.html"> 

                        <figure> 

                            <img src="obrazky/Pixelfed_wide.webp" width="350" height="200" alt="Pixelfed"> 

                            <center> 

                                <figcaption>Pixelfed</figcaption> 

                            </center> 

                        </figure> 

                    </a> 

                </td> 

            </tr> 

        </table> 

    </center> 
    ```